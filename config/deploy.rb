# require 'bundler/capistrano'
# require 'rvm/capistrano'
require "rvm/capistrano"
require "bundler/capistrano"

set :stages, %w{production}
set :default_stage, "production"
require 'capistrano/ext/multistage'

set :application, "cingo"
set :user, "deploy"
set :deploy_to, "/home/deploy/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false
set :domain, "ec2-54-85-41-230.compute-1.amazonaws.com"

set :scm, "git"
set :scm_passphrase, "cingo123"
set :repository, "git@bitbucket.org:pandurang90/cingo.git"
set :keep_releases, 3
set :use_sudo, false
role :app , domain
role :web, domain
role :db, domain, :primary => true

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_rsa")]

 set :rvm_ruby_string, "ruby-1.9.3-p545@cingo"
# set :rvm_type, :user
set :deploy_via, :remote_cache

set :normalize_asset_timestamps, false
before 'deploy:assets:precompile', 'deploy:create_symlinks'
after 'deploy:update_code', 'deploy:migrate'
# after 'deploy:update_code', 'deploy:assets:precompile'
# after 'deploy:restart', 'deploy:stop','deploy:start'
# after "deploy:finalize_update", "deploy:web:disable"
after "deploy:update_code", "deploy:cleanup"


namespace :deploy do
  desc "reload the database with seed data"
  task :seed do
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end

  # desc "Start the Thin processes"
  # task :start do
  #   run "cd #{current_path}; bundle exec thin start -C /home/deploy/cingo.yml"
  # end

  # desc "Stop the Thin processes"
  # task :stop do
  #   run "cd #{current_path}; bundle exec thin stop -C /home/deploy/cingo.yml"
  # end

  # desc "Restart the Thin processes"
  # task :restart do
  #   run "cd /home/deploy; bundle exec thin restart -C /home/deploy/cingo.yml"
  # end

  desc 'Copy database.yml from shared to current folder'
  task :create_symlinks, :roles => :app, :except => {:no_release => true} do
    puts "Running symlinks"
    run "ln -s #{shared_path}/config/database.yml #{current_release}/config/database.yml"
  end

  desc "Migrating the database"
  task :migrate, :roles => :db do
    run "cd #{release_path} && RAILS_ENV=#{rails_env} bundle exec rake db:migrate --trace"
  end


  namespace :assets do
    task :precompile, :roles => :web, :except => { :no_release => true } do
       run %Q{cd #{release_path} && RAILS_ENV=#{rails_env} bundle exec rake assets:clean && RAILS_ENV=#{rails_env} bundle exec rake assets:precompile --trace}
    end
  end

end
