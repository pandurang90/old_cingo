class CreateWebsites < ActiveRecord::Migration
  def change
    create_table :websites do |t|
      t.string :url
      t.string :user_name
      t.string :password

      t.timestamps
    end
  end
end
