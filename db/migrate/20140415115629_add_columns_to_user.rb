class AddColumnsToUser < ActiveRecord::Migration
  def change
  	add_column :users, :export, :string
	add_column :users, :cert, :text  	
  end
end
