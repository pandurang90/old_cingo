class HomeController < ApplicationController
  def index
  	Rails.logger.info request.inspect
  	Rails.logger.info request.env["X-SSL-Client-S-DN"] if request.env["X-SSL-Client-S-DN"].present?
  	redirect_to websites_path if current_user
  end
end
