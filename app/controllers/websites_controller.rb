class WebsitesController < ApplicationController

	before_filter :authenticate_user!, except: [:auto_fill]

  def index
  end

  def setup    
  end

  def auto_fill
    Rails.logger.info request.inspect
    pcert = request.env["HTTP_X_SSL_CLIENT_S_DN"]
    if pcert.present?
      @cert = true
      Rails.logger.info request.inspect
      pcert = pcert.gsub("-----BEGIN CERTIFICATE-----","").gsub("-----END CERTIFICATE-----","").gsub("\n","").gsub("\t","")
      Rails.logger.info pcert
      @user = User.where(email: params[:e], cert: pcert).last      
      if @user.present?
        urls = @user.websites.collect(&:url)
        extract_url = extract_domain(params[:url])
        #[params[:url].split("/")[2].split(".")[0],params[:url].split("/")[2].split(".")[1]]
        @website = @user.websites.find_all{|website| website.url.include? extract_url}.last       
      end
    else
      @cert = nil
    end
  	respond_to do |format|
  		format.js {}
  	end
  end


  def extract_domain(url)
    require 'uri'
    domain = URI.parse(url).host.split('.')

    raise Exception.new("Invalid host") if domain.length < 2

    if (domain[-1].length == 2) and (domain[-2].length == 3 || domain[-2].length == 2)
      return domain[-3..-1].join('.')
    else
      return domain[-2..-1].join('.')
    end
  end

  def get_info
  	
  	respond_to do |format|
  		format.js {}
  	end
  end

  def new
  	@website = current_user.websites.new
  end

  def create
  	@website = current_user.websites.new(params[:website])
  	if @website.save
  		flash[:notice] = "Website added successfully"
      redirect_to setup_path
  	else
  		render "new"
  	end
  end

  def edit
  	@website = current_user.websites.find(params[:id])
  end

  def update
  	@website = current_user.websites.find(params[:id])
  	if @website.update_attributes(params[:website])
      flash[:notice] = "Website updated successfully"
  		redirect_to setup_path
  	else
  		render "edit"
  	end
  end

  def destroy
  	@website = current_user.websites.find(params[:id])
  	@website.destroy
    flash[:notice] = "Website removed successfully"
  	redirect_to setup_path	
  end
end
