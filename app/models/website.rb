class Website < ActiveRecord::Base
  attr_accessible :url, :user_name,:password,:salt
  belongs_to :user

  validates :url, :presence => true
  #validates :url, url: true
  validates_format_of :url ,with: /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix

  validates :user_name, :presence => true
  validates :password, :presence => true

  before_validation :check_url
  before_save :encrypt_password

  def check_url
  	if self.url.include? "http"
  	else
  		self.url = "http://#{url}"
  	end
  end

  def encrypt_password
    self.salt = Devise.friendly_token.to_s+Devise.friendly_token.to_s if !self.salt.present?
    a=ActiveSupport::MessageEncryptor.new(self.salt)
    self.password = a.encrypt_and_sign(self.password) 
  end

  def decrypt
    a=ActiveSupport::MessageEncryptor.new(self.salt)
    a.decrypt_and_verify(self.password)
  end
end
