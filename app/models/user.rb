class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  CERT_DIR = "/home/deploy/cingo/newCA/"

  # Setup accessible (or protected) attributes for your model
  attr_accessible :cert_name,:export,:cert,:country_code,:email, :password, :password_confirmation, :remember_me,:first_name, :last_name,:phone_number
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :phone_number, :presence => true 
  validates :country_code, :presence => true

  before_save :set_phone_number
  has_many :websites, :dependent => :destroy
  # attr_accessible :title, :body
  #before_destroy :remove_files

  after_create :send_cert

  def set_phone_number
    if self.phone_number.present?
      self.phone_number = self.phone_number.gsub("-","").gsub("(","").gsub(")","").gsub(" ","")
    end
    self.export = self.password if !self.export.present? && self.new_record?
  end

  def send_cert
    create_p12
    Notice.welcome(self.id).deliver
  end

  def phone
    "#{self.country_code}-#{self.phone_number}"
  end

  def name
    "#{first_name} #{last_name}"
  end

  def create_p12
    subj = "/C=US/ST=Texas/L=Austin/O=Cingo/OU=Cingo/CN=#{self.name} (#{self.email})/emailAddress=#{self.email}"
    self.cert_name = "#{self.first_name.downcase}_#{self.last_name.downcase}"
    self.save
    dir_name  = "#{CERT_DIR}#{self.id}"
    Dir.mkdir(dir_name) unless File.directory?(dir_name)
    create_cert(subj)
    sign_cert
    generate_p12
    export_cert_to_db
    #system("openssl pkcs12 -export -in #{CERT_DIR}server_cert.pem -inkey #{CERT_DIR}new_server_key.pem -out #{CERT_DIR}#{self.id}/#{self.cert_name}.p12 -passout pass:#{self.export}")        
  end

  def create_cert(subj)
    system("openssl req -new -sha1 -newkey rsa:1024 -nodes -keyout #{CERT_DIR}#{self.id}/#{self.cert_name}.key -out #{CERT_DIR}#{self.id}/#{self.cert_name}.csr -subj '#{subj}'")
  end

  def sign_cert
    system("openssl ca -batch -config /usr/lib/ssl/openssl.cnf -policy policy_anything -extensions ssl_client -out #{CERT_DIR}#{self.id}/#{self.cert_name}.crt -infiles #{CERT_DIR}#{self.id}/#{self.cert_name}.csr")
  end

  def generate_p12
    system("openssl pkcs12 -export -clcerts -in #{CERT_DIR}#{self.id}/#{self.cert_name}.crt -certfile #{CERT_DIR}demoCA/cacert.pem -inkey #{CERT_DIR}#{self.id}/#{self.cert_name}.key -out #{CERT_DIR}#{self.id}/#{self.cert_name}.p12 -name '#{self.name}' -passout pass:#{self.export} ")
  end

  def export_cert_to_db
    f=File.read("#{CERT_DIR}#{self.id}/#{self.cert_name}.crt")
    self.cert = f.split("BEGIN CERTIFICATE")[1].split("END CERTIFICATE")[0].gsub("-","").gsub("\n","")
    self.save
  end
end
