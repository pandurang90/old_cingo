module ApplicationHelper

	def get_link
		secret_key = current_user.email
		key = current_user.email.split("@").first
		encrypted = "#{Encryptor.encrypt(secret_key,key: key)}--#{key}".html_safe
			# var ;
		"javascript:(function(){
				var h,l,s;
				var e='#{secret_key}';
				var _CINGO={
					url:'https://ec2-54-85-41-230.compute-1.amazonaws.com/auto_fill.js',
					l:encodeURIComponent(location.href),
					s:document.createElement('script'),
					h:document.getElementsByTagName('head')[0]
				};
				var s= _CINGO.s;
				s.src=_CINGO.url+'?url='+_CINGO.l+'&e='+e;
				document.getElementsByTagName('head')[0].appendChild(s);					
				})()"

	end

	def extract_domain(url)
    require 'uri'
    domain = URI.parse(url).host.split('.')
    raise Exception.new("Invalid host") if domain.length < 2
    URI.parse(url).host
  end

  def active_menu(controller,action)
  	return "active" if params[:controller] == controller && params[:action] == action
  	""
  end
end
