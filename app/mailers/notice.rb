class Notice < ActionMailer::Base
  default from: "admin@cingo.me"

  def welcome(user_id)
  	@user = User.find(user_id)
  	#attachments["#{@user.cert_name}.p12"] = File.read("#{User::CERT_DIR}#{user_id}/#{@user.cert_name}.p12")
  	
  	attachments["#{@user.cert_name}.p12"] = {:data => File.read("#{User::CERT_DIR}#{user_id}/#{@user.cert_name}.p12"),
                                :mime_type => 'application/x-pkcs12' }
  	mail(to: @user.email, subject: "Welcome to CINGO")
  end
end
